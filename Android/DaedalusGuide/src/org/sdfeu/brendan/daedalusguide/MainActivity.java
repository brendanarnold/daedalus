package org.sdfeu.brendan.daedalusguide;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
// Network imports
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

//android.location GPS
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
// Other imports
import com.example.myfirstapp.R;
import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends FragmentActivity
implements
GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener,
LocationListener
{

	private final String SERVER_URL = "https://daedalus-maze.eu01.aws.af.cm";
	private final String SECRET = "moresecret";
	//private final String SERVER_URL = "http://192.168.1.66:8000";
	//private final String SECRET = "secret";
	private final String ACTOR = "icarus";
	private final String ACTOR_POST_KEY = "actor";
	private final String DIRECTION_POST_KEY = "direction";
	private final String SECRET_POST_KEY = "secret";
	// 10 seconds means will have to travel for about 5 1/2 hrs to escape
	// 10 metres means will have to to walk 20km to escape
	private static final int UPDATE_DIST = 10;
	// Update frequency in seconds
    public static final int UPDATE_INTERVAL_IN_SECONDS = 10;
 // The fastest update frequency, in seconds
    private static final int FASTEST_INTERVAL_IN_SECONDS = 1;
	
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    // Milliseconds per second
	
    private static final int MILLISECONDS_PER_SECOND = 1000;
    // Update frequency in milliseconds
    private static final long UPDATE_INTERVAL =
            MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // A fast frequency ceiling in milliseconds
    private static final long FASTEST_INTERVAL =
            MILLISECONDS_PER_SECOND * FASTEST_INTERVAL_IN_SECONDS;
	private static final double UNDEF_COORD = 1000.0;
	private static final long UNDEF_TIME = -1;
	
	
	private TextView logTextView;
	private int logCount;
	LocationClient mLocationClient;
	LocationRequest mLocationRequest;
	boolean mUpdatesRequested;
    SharedPreferences mPrefs;
    Editor mEditor;
    double prevLat = UNDEF_COORD;
    double prevLng = UNDEF_COORD;
    long prevTime = UNDEF_TIME;
    
    
    
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_main);
        this.logTextView = (TextView)findViewById(R.id.log_text_view);
        this.logTextView.setMovementMethod(new ScrollingMovementMethod());
        this.logCount = 0;
         
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        
        mLocationClient = new LocationClient(this, this, this);
        
          
          
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
    protected void onPause(){
    	super.onPause();
    }
    
    @Override
    protected void onStart() {
    	// Connect the client.
    	if (this.servicesConnected()) {
    		mLocationClient.connect();
    		logEntry("Connecting ...");
    	}
    	super.onStart();
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    }
    
    
    @Override
    protected void onStop() {
    	mLocationClient.disconnect();
        logEntry("Updates halted");
        super.onStop();
    }
    
    
    public void goNorth(View view) {
    	sendDirn("n");
    }
    
    public void goSouth(View view) {
    	sendDirn("s");
    }
    
    public void goEast(View view) {
    	sendDirn("e");
    }
    
    public void goWest(View view) {
    	sendDirn("w");
    }
    
    public void logEntry(String newEntry) {
    	this.logTextView.append(String.format("%d : %s\n", this.logCount, newEntry));
    	this.logCount++;
    }
    
    
    // Code for network
    
    public void sendDirn(String dirn) {
        ConnectivityManager connMgr = (ConnectivityManager) 
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new SendDirnTask().execute(dirn);
        } else {
            this.logEntry(String.format("No network: %s", dirn));
        }
    }
    
    private class SendDirnTask extends AsyncTask<String, Void, String> {
    	@Override
    	protected String doInBackground(String... dirns) {
    		try {
    			return sendGuidance(dirns[0]);
    		} catch (IOException e) {
    			return String.format("Network available but failed to send: %s", dirns[0]);
    		}
    	}
    	@Override
    	protected void onPostExecute(String result) {
    		logEntry(result);
    	}
    }
    
    private String sendGuidance(String dirn) throws IOException {
    	HttpResponse response;
    	DefaultHttpClient httpClient;
    	HttpPost req = new HttpPost(SERVER_URL);
    	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair(ACTOR_POST_KEY, ACTOR));
        nameValuePairs.add(new BasicNameValuePair(SECRET_POST_KEY, SECRET));
        nameValuePairs.add(new BasicNameValuePair(DIRECTION_POST_KEY, dirn));
        req.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        
        httpClient = (DefaultHttpClient) WebClientDevWrapper.getNewHttpClient();
    	
    	try {
	    	response = httpClient.execute(req);
    	} catch (Exception e) {
    		return String.format("Request failed: %s", dirn);
    	}
    	int status = response.getStatusLine().getStatusCode();
    	if (status != 200) {
    		return String.format("Failed: %s",  dirn);
    	} else {
    		return String.format("Success: %s",  dirn);
    	}
    }
    
    // CODE FOR GPS
    
 // Define a DialogFragment that displays the error dialog
    public static class ErrorDialogFragment extends DialogFragment {
        // Global field to contain the error dialog
        private Dialog mDialog;
        // Default constructor. Sets the dialog field to null
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }
        // Set the dialog to display
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }
        // Return a Dialog to the DialogFragment.
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }
    
    /*
     * Handle results returned to the FragmentActivity
     * by Google Play services
     */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
        switch (requestCode) {
            
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                switch (resultCode) {
                    case Activity.RESULT_OK :
                    /*
                     * Try the request again
                     */
                    break;
                }
            
        }
     }
    
    private boolean servicesConnected() {
        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(this);
        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Location Updates",
                    "Google Play services is available.");
            // Continue
            return true;
        // Google Play services was not available for some reason
        } else {
            // Get the error dialog from Google Play services
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                    resultCode,
                    this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            // If Google Play services can provide an error dialog
            if (errorDialog != null) {
                // Create a new DialogFragment for the error dialog
                ErrorDialogFragment errorFragment =
                        new ErrorDialogFragment();
                // Set the dialog in the DialogFragment
                errorFragment.setDialog(errorDialog);
                // Show the error dialog in the DialogFragment
                errorFragment.show(getSupportFragmentManager(),
                        "Location Updates");
            }
            return false;
        }
    }
    
    
    
    
    @Override
    public void onDisconnected() {
        // Display the connection status
        Toast.makeText(this, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }
    
   /*
    * Called by Location Services if the attempt to
    * Location Services fails.
    */
   @Override
   public void onConnectionFailed(ConnectionResult connectionResult) {
       /*
        * Google Play services can resolve some errors it detects.
        * If the error has a resolution, try sending an Intent to
        * start a Google Play services activity that can resolve
        * error.
        */
       if (connectionResult.hasResolution()) {
           try {
               // Start an Activity that tries to resolve the error
               connectionResult.startResolutionForResult(
                       this,
                       CONNECTION_FAILURE_RESOLUTION_REQUEST);
               /*
                * Thrown if Google Play services cancelled the original
                * PendingIntent
                */
           } catch (IntentSender.SendIntentException e) {
               // Log the error
               e.printStackTrace();
           }
       } else {
           /*
            * If no resolution is available, display a dialog to the
            * user with the error.
            */
           logEntry(String.format("Connection failed: %d", connectionResult.getErrorCode()));
       }
   }
   
   @Override
   public void onConnected(Bundle dataBundle) {
       // Display the connection status
       Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
       // If already requested, start periodic updates
       
       mLocationClient.requestLocationUpdates(mLocationRequest, this);
       
   }
   
   // Define the callback method that receives location updates
   @Override
   public void onLocationChanged(Location location) {
       // Report to the UI that the location was updated
       double lat = location.getLatitude();
       double lng = location.getLongitude();
   	   long t = location.getTime();
   	
       if (prevTime == UNDEF_TIME) {
       		prevLat = lat;
       		prevLng = lng;
       		prevTime = t;
       } else {
	       	float[] results = new float[3];
	       	Location.distanceBetween(prevLat, prevLng, lat, lng, results);
	       	//logEntry(String.format("Distance: %.1f @ %.1f", results[0], results[1]));
	       	if ((results[0] >= (float)UPDATE_DIST)
	       			&& ((t - prevTime) > UPDATE_INTERVAL)) {
	       		String dirn;
	       		double angle = Math.atan2(lat - prevLat, lng - prevLng);
	       		double deg45 = Math.PI / 4;
	               double deg135 = Math.PI * 3 / 4;
	               if (angle >= -deg45 && angle < deg45) {
	                   dirn = "e";
	               } else if (angle >= -deg135 && angle < -deg45) {
	                   dirn = "s";
	               } else if (angle >= deg45 && angle < deg135) {
	                   dirn = "n";
	               } else {
	                   dirn = "w";
	               }
	       		sendDirn(dirn);
	       		//logEntry(dirn);
	       		prevLat = lat;
	           	prevLng = lng;
	           	prevTime = t;
	       	}
       		
       }
       
       
   }
   
  
    
    
}
    
    
    

    
    
    






