$(document).ready(function() {
    
    // Some variables that will need to be changed if the UI is altered
    // Space around the maze: Top, right, bottom, left 
    var MAZE_MARGIN = { 't' : 50,  'r' : 100,  'b' : 130, 'l' : 50 };
    // Size of the entire model
    var CRETE_SIZE = { 'w' : 4151, 'h' : 4181 };
    var SUN_SIZE = { 'w' : 42, 'h' : 42};

    var daedalus = $("#daedalus");
    var icarus = $("#icarus");
    var minotaur = $("#minotaur");
    var sun = $("#sun");

    if (document.URL.match("localhost") == "localhost") {
        var socketHost = 'http://localhost:8000';
    } else {
        var socketHost = 'https://daedalus-maze.eu01.aws.af.cm:80';
    }
    var socket = io.connect(socketHost);

    socket.on('connect', function() {
        // Identify self as client
        socket.emit('clientType',  { 'type' : 'watcher' });
    });

    socket.on('posUpdate', function (data) {
        daedalus.css({"left": data.daedalusPosX + "px", "top" : data.daedalusPosY + "px"});
        minotaur.css({"left": data.minotaurPosX + "px", "top": data.minotaurPosY + "px"});
        icarus.css({"left": data.icarusPosX + "px", "top": data.icarusPosY + "px"});
        sun.css({"left": sunNrmPosXToPx(data.sunNrmPosX) + "px", "top": sunNrmPosYToPx(data.sunNrmPosY) + "px"});
        console.log(data);
    });

    function sunNrmPosXToPx(nrmPosX) {
        return Math.floor(nrmPosX * (CRETE_SIZE.w + SUN_SIZE.w) - MAZE_MARGIN.l) - SUN_SIZE.w;
    }

    function sunNrmPosYToPx(nrmPosY) {
        return Math.floor(nrmPosY * CRETE_SIZE.h - MAZE_MARGIN.t);
    }

});
