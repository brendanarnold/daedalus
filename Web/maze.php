<?php include_once 'config.php' ?>

<!DOCTYPE html>
<html>
  <head>
    <title>Daedalus</title>


    <style type="text/css">

body {
  margin: 0px;
  padding: 0px;
}

#crete {
  margin: 50px 0px 0px 50px;
  position: relative;
  width: 4101px;
  height: 4131px;
}

#knossos {
  position: absolute;
  top: 4001px;
  left: 4001px;
}

#knossos > .label {
  margin-top: -5px;
}

#labyrinth {
}

#daedalus {
  position: absolute;
  top: 827px;
  left: 1057px;
}

#icarus {
  position: absolute;
  top: 827px;
  left: 1057px;
}

#minotaur {
  position: absolute;
  top: 2971px;
  left: 1933px;
}

#sun {
  position: absolute;
  top: 2000px;
  left: -1000px;
  text-align: center;
}

#moon {
  position: absolute;
  top: 2000px;
  left: -1000px;
  text-align: center;
}
.pixel {
  width: 1px;
  height: 1px;
  background-color: red;
}

.label {
  margin-top: 2px;
  padding: 2px;
  background-color: white;
  border: 1px solid black;
  text-transform: uppercase;
  font-family: sans-serif;
  font-size: 12px;
  display: inline-block;
}


    
    </style>

  </head>
  <body>

    
  
    <div id="crete">
      <div id="moon">
        <svg width="34px" height="34px" style="display: block;" xmlns="http://www.w3.org/2000/svg" version="1.1">
          <circle id="moon" cx="17" cy="17" r="16" stroke="black" stroke-width="1" fill="white" />
        </svg>
        <div class="label">Moon</div>
      </div>
      <div id="sun">
        <svg width="42px" height="42px" style="display: block;" xmlns="http://www.w3.org/2000/svg" version="1.1">
          <circle id="sun" cx="21" cy="21" r="20" stroke="black" stroke-width="1" fill="white" />
        </svg>
        <div class="label">Sun</div>
      </div>
      <div id="daedalus">
        <div class="pixel"></div> 
        <div class="label">Daedalus</div>
      </div>
      <div id="minotaur">
        <div class="pixel"></div> 
        <div class="label">Minotaur</div>
      </div>
      <div id="icarus">
        <div class="pixel"></div>
        <div class="label">Icarus</div>
      </div>
      <img src="maze.png" id="labyrinth" />
      <div id="knossos">
        <img src="knossos.svg" />
        <div class="label">Knossos</div>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?php echo $serverHost ?>socket.io/socket.io.js" type="text/javascript"></script>
    <script src="move_actors.js" type="text/javascript" charset="utf-8"></script>


  </body>
</html>
