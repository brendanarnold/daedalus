<?php include_once 'config.php' ?>

<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.css" />

    <title>Daedalus project</title>

    <style type="text/css">

h1 {
  margin-top: 150px;
}

p {
  line-height: 2;
}

.mainlink {
  text-transform: uppercase;
}
    
    </style>

  </head>
  <body>

  <div class="row">
    <div class="small-12 columns">

      <h1>Daedalus</h1>
  
      <p>Daedalus created the labyrinth at Knossos to contain the Minataur but was thrown into it himself. He and his son Icarus have been trying to escape since.</p>

      <h2>About</h2>

      <p>Daedalus is an evolving real time art project which uses mobile apps to track the movements and choices of various real people to influence actors in an virtual maze. Each actor has different behaviours which correspond to their stories in Greek legend. As they progress a script is written which documents their ordeals &ndash; initially this is updated when the code is updated but eventually the virtual actors will start to write the script themselves.</p>

      <p class="mainlink"><a href="maze.php">View the maze</a></p>

      <p class="mainlink"><a href="script.php">View the script</a></p>

      <p class="mainlink"><a href="/">Main site</a></p>

    </div>
  </div>
  
</body>
</html>
