<?php include_once 'config.php' ?>

<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">

    <link rel="stylesheet" href="css/normalize.css" />
    <link rel="stylesheet" href="css/foundation.css" />

    <title>Daedalus project - Script</title>

    <script type="text/javascript">
    
    </script>

    <style type="text/css">

dl {
  margin-top: 150px;
}

p {
  line-height: 2;
}

.mainlink {
  text-transform: uppercase;
}

dt {
  text-transform: uppercase;
  margin-bottom: 5px;
}

dd {
  margin-bottom: 15px;
  font-style: italic;
}

dt.direction {
  text-transform: none;
  /*text-align: center;*/
  margin-bottom: 15px;
}
    
    </style>

  </head>
  <body>

  <div class="row">
    <div class="small-12 columns">

<dl>
  <dt>Minos</dt>
  <dd>Build me a labyrinth to hold the son of my wife.</dd>
  <dt>Daedalus</dt>
  <dd>OK</dd>
  <dt>Daedalus</dt>
  <dd>I have built the maze but I am trapped in it.</dd>
  <dt>Icarus</dt>
  <dd>The sun has appeared from behind the clouds.</dd>
  <dt>Daedalus</dt>
  <dd>I can now keep track of time with the sun in the sky.</dd>
  <dt>Daedalus</dt>
  <dd>I now know who I am</dd>
  <dt>Icarus</dt>
  <dd>As do I</dd>
  <dt>Minotaur</dt>
  <dd>As do I</dd>
  <dt>Sun</dt>
  <dd>As do I</dd>
  <dt>Daedalus</dt>
  <dd>My ears feel clear when before they were wool filled</dd>
</dl>

    </div>
  </div>

</body>
</html>
  
