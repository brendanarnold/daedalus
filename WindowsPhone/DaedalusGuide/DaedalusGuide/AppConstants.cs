﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DaedalusGuide
{
    public static class AppConstants
    {
        public static string GUIDANCE_DIRN_POST_KEY = "direction";
        public static string ACTOR_POST_KEY = "actor";
        public static string GUIDANCE_SECRET_POST_KEY = "secret";
        public static int FREQUENCY_UPDATE_LIMIT = 10; // Seconds
        public static int MIN_DISTANCE_UPDATE = 10; // Metres
#if LIVE
        public static string HOST = "http://daedalusproject.azurewebsites.net/";
        public static string SECRET = "moresecret";
#else
        public static string HOST = "http://192.168.1.66:8000/";
        public static string SECRET = "secret";
#endif

    }
}
