﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DaedalusGuide
{
    public enum Actors
    {
        Icarus,
        Daedalus,
        Minotaur,
        Sun,
        Moon,
    }

    public static class ActorsLookup
    {
        public static Dictionary<Actors, string> PostString = new Dictionary<Actors, string>()
        {
            { Actors.Daedalus, "daedalus" },
            { Actors.Icarus, "icarus" },
            { Actors.Minotaur, "minotaur" },
            { Actors.Moon, "moon" },
            { Actors.Sun, "sun" },
        };
    }


}
