﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DaedalusGuide
{
    public class ViewModel : INotifyPropertyChanged
    {

        private Actors _actor = Actors.Daedalus;
        public Actors Actor {
            get { return this._actor; }
            set
            {
                if (this._actor != value)
                {
                    this._actor = value;
                    this.NotifyPropertyChanged("Actor");
                }
            }
        }

        public ViewModel()
        {
            this.Relay = new GuideRelay();
            this.Tracker = new Tracker();
            this.Log = new ObservableCollection<string>();
        }

        public GuideRelay Relay;
        public Tracker Tracker;

        public async Task<bool> SendGuidance(string dirn) 
        {
            bool res = await this.Relay.SendGuidanceTaskAsync(dirn, this.Actor);
            if (res)
            {
                this.LogEntry(String.Format("{0} SUCCESS: {1}", this.Log.Count, dirn));
                return true;
            }
            else
            {
                this.LogEntry(String.Format("{0} FAIL: {1}", this.Log.Count, dirn));
                return false;
            }
        }

        public ObservableCollection<string> Log { get; set; }

        public void LogEntry(string entry) 
        {
            this.Log.Insert(0, entry);
        }


        public void ChangeActor(Actors actor)
        {
            this.Actor = actor;
        }



        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        internal void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion


    }
}
