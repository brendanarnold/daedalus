﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using DaedalusGuide.Resources;

namespace DaedalusGuide
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            this.DataContext = App.VM;

            
        }

        

        private void scrollContainer_Flick(object sender, FlickGestureEventArgs e) 
        {
            if (App.VM.Actor == Actors.Daedalus)
            {
                if (e.HorizontalVelocity < 600)
                {
                    App.VM.ChangeActor(Actors.Icarus);
                }
            }
            else if (App.VM.Actor == Actors.Icarus)
            {
                if (e.HorizontalVelocity > -600)
                {
                    App.VM.ChangeActor(Actors.Daedalus);
                }

            }
        }


       

        private void northDaedalusBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.VM.SendGuidance("n");
        }

        private void westDaedalusBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.VM.SendGuidance("w");
        }

        private void eastDaedalusBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.VM.SendGuidance("e");
        }

        private void southDaedalusBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.VM.SendGuidance("s");
        }

        private void northIcarusBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.VM.SendGuidance("n");
        }

        private void westIcarusBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.VM.SendGuidance("w");
        }

        private void eastIcarusBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.VM.SendGuidance("e");
        }

        private void southIcarusBtn_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.VM.SendGuidance("s");
        }

        private void Button_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.VM.ChangeActor(Actors.Daedalus);
        }

        private void Button_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            App.VM.ChangeActor(Actors.Icarus);
        }

        

        
    }
}