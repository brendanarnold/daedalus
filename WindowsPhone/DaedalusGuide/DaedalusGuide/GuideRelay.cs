﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;

namespace DaedalusGuide
{
    public class GuideRelay
    {

        public async System.Threading.Tasks.Task<bool> SendGuidanceTaskAsync(string dirn, Actors actor)
        {
            FormUrlEncodedContent postContent = new FormUrlEncodedContent(new Dictionary<string, string>() 
            {
                { AppConstants.ACTOR_POST_KEY, ActorsLookup.PostString[actor] },
                { AppConstants.GUIDANCE_DIRN_POST_KEY, dirn },
                { AppConstants.GUIDANCE_SECRET_POST_KEY, AppConstants.SECRET },
            });
            HttpClient http = new HttpClient();
            HttpResponseMessage res = await http.PostAsync(AppConstants.HOST, postContent);
            return (res.StatusCode == HttpStatusCode.OK);
        }


    }
}
