﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DaedalusGuide
{
    public class Tracker
    {
        public GeoCoordinateWatcher watcher;
        public long prevUpdate = 0;
        GeoPositionChangedEventArgs<GeoCoordinate> prevDataPoint;

        public Tracker()
        {
            this.watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
            this.watcher.PositionChanged += watcher_PositionChanged;
            this.watcher.Start();
        }

        void watcher_PositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> e)
        {
            
            if (this.prevDataPoint == null) 
            {
                this.prevDataPoint = e;
            } 
            else if (e.Position.Timestamp.Ticks - prevDataPoint.Position.Timestamp.Ticks > AppConstants.FREQUENCY_UPDATE_LIMIT * 10000)
            {

                GeoCoordinate curr = new GeoCoordinate(e.Position.Location.Latitude, e.Position.Location.Longitude);
                GeoCoordinate prev = new GeoCoordinate(this.prevDataPoint.Position.Location.Latitude, this.prevDataPoint.Position.Location.Longitude);
                double dist = curr.GetDistanceTo(prev);
                if (dist >= AppConstants.MIN_DISTANCE_UPDATE)
                {
                    string dirn;
                    double x0 = this.prevDataPoint.Position.Location.Longitude;
                    double y0 = this.prevDataPoint.Position.Location.Latitude;
                    double x1 = e.Position.Location.Longitude;
                    double y1 = e.Position.Location.Latitude;
                    double angle = Math.Atan2(y1 - y0, x1 - x0);
                    double deg45 = Math.PI / 4;
                    double deg135 = Math.PI * 3 / 4;

                    if (angle >= -deg45 && angle < deg45)
                    {
                        dirn = "e";
                    }
                    else if (angle >= -deg135 && angle < -deg45)
                    {
                        dirn = "s";
                    }
                    else if (angle >= deg45 && angle < deg135)
                    {
                        dirn = "n";
                    }
                    else
                    {
                        dirn = "w";
                    }

                    App.VM.SendGuidance(dirn);
                    this.prevDataPoint = e;
                }
                
                
            }
            
            
        }


    }
}
