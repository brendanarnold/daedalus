//
//  LocationManager.swift
//  Daedalus
//
//  Created by Brendan Arnold on 28/10/2018.
//  Copyright © 2018 Brendan Arnold. All rights reserved.
//

import CoreLocation

class LocationManager {
    static let shared = CLLocationManager()
    
    private init() {}
}

