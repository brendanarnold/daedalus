//
//  ViewController.swift
//  Daedalus
//
//  Created by Brendan Arnold on 26/10/2018.
//  Copyright © 2018 Brendan Arnold. All rights reserved.
//

import UIKit
import CoreLocation


enum Actor: String {
    case Daedalus = "daedalus", Icarus = "icarus", Minotaur = "minataur", Sun = "sun"
}

let secret = "Booyah"
let isLive = true

let actorDirectionUrl = isLive ? "http://daedalus.tangibl.co.uk" : "http://192.168.1.4:8000"
let mazePositionUrl = isLive ? "" : ""

let actor = Actor.Daedalus

enum Direction: String {
    case North = "n", South = "s", East = "e", West = "w"
}



func direct(_ actor: Actor, _ direction: Direction, cb: () -> Void) {
    print("\(actor) sent \(direction)")
    let url = URL(string: actorDirectionUrl)
    var request = URLRequest(url: url!)
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
    request.httpMethod = "POST"
    let postString = "direction=\(direction.rawValue)&actor=\(actor.rawValue)&secret=\(secret)"
    request.httpBody = postString.data(using: .utf8)
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard error == nil else {
            print(error ?? "No error given")
            return
        }
        print ("Sent OK")
    }
    task.resume()
}


class ViewController: UIViewController, CLLocationManagerDelegate {

    @IBAction func northButton(_ sender: Any) {
        direct(actor, .North) {}
    }
    @IBAction func southButton(_ sender: Any) {
        direct(actor, .South) {}
    }
    @IBAction func eastButton(_ sender: Any) {
        direct(actor, .East) {}
    }
    @IBAction func westButton(_ sender: Any) {
        direct(actor, .West) {}
    }
    
    let locationManager = LocationManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        startRecievingSignficantLocationChanges()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func startRecievingSignficantLocationChanges() {
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if authorizationStatus != .authorizedAlways {
            return
        }
        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
            return
        }
        locationManager.delegate = self
        locationManager.startMonitoringSignificantLocationChanges()
//        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.activityType = .fitness
        locationManager.startUpdatingHeading()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        print("Heading updated \(newHeading)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Location updated \(locations)")
    }
    

}

