// Singleton pattern from http://www.youtube.com/watch?feature=player_detailpage&v=hQVTIJBZook#t=1701
module.exports = (function() {

    var _ = require('underscore');

    // Knossos palace is at lat 35.297894, long 25.163387
    var _lat = 35.297894;
    var _lng = 25.163387;

    function _dsin(deg) {
        return Math.sin(deg * Math.PI / 180.0);
    }

    function _dcos(deg) {
        return Math.cos(deg * Math.PI / 180.0);
    }

    

    return {

        name: 'SUN',

        nrmPos: [0, 0], // position of the sun relative to the map of crete - pixels figured out client-side

        updateNrmPos: function() {
            // Get Julian date - adapted from http://jesusnjim.com/julian-gregorian.js
            var now = new Date();
            var a = Math.floor( (14 - now.getMonth() - 1) / 12 );
            var y = now.getFullYear() + 4800 - a;
            var m = now.getMonth() + 12 * a - 2;
            var julDate =  now.getDate() + Math.floor((153 * m + 2) / 5) + (365 * y) + Math.floor(y / 4) - Math.floor(y / 100) + Math.floor(y / 400) - 32045 + ((now.getHours() - 12) / 24) + (now.getMinutes() / 1440) + (now.getSeconds() / 86400) + (now.getMilliseconds() / 86400000);


            // Equations from http://en.wikipedia.org/wiki/Sunrise_equation
            var julCycleSince2000 = Math.floor(julDate) - 2451545;
            var solarNoon = 2451545.0009 + _lng / 360.0 + julCycleSince2000;
            var meanAnom = (357.5291 + 0.98560028 * (solarNoon - 2451545)) % 360;
            var eqnOfCenter = 1.9148 * _dsin(meanAnom) + 0.02 * _dsin(2 * meanAnom) + 0.0003 * _dsin(3 * meanAnom);
            var eclipticLong = (meanAnom + 102.9372 + eqnOfCenter + 180) % 360;
            var solarTransit = solarNoon + 0.0053 * _dsin(meanAnom) - 0.0069 * _dsin(2 * eclipticLong);
            var declinationSun = (180.0 / Math.PI) * Math.asin( _dsin(eclipticLong) * _dsin(23.45) );
            var hourAngle = (180.0 / Math.PI) * Math.acos( 
                ( _dsin(-0.83) - _dsin(_lat) * _dsin(declinationSun) ) 
                / ( _dcos(_lat) * _dcos(declinationSun) ) 
            );
            var sunset = 2451545.0009 + (hourAngle + _lng) / 360.0 + julCycleSince2000 + 0.0053 * _dsin(meanAnom) - 0.0069 * _dsin(2 * eclipticLong);
            var sunrise = solarTransit - (sunset - solarTransit);

            // Annoyingly the Julian system rolls over at noon and not
            // at midnight so will normalise to zero at midnight
            var sunriseFracDay = (sunrise % 1) - 0.5;
            var sunsetFracDay = (sunset % 1) + 0.5;
            var nowFracDay = (julDate % 1) >= 0.5 ? (julDate % 1) - 0.5 : (julDate % 1) + 0.5;
            if (nowFracDay > sunriseFracDay && nowFracDay < sunsetFracDay) {
                this.nrmPos[0] = (nowFracDay - sunriseFracDay) / (sunsetFracDay - sunriseFracDay);
            } else {
                this.nrmPos[0] = -1.0;
            }
        },



    }

})();


