// Singleton pattern from http://www.youtube.com/watch?feature=player_detailpage&v=hQVTIJBZook#t=1701
module.exports = (function() {

    var PNG = require('png-js');
    var _width = 4001;
    var _height = 4001;
    // var _width = 55;
    // var _height = 47;
    var _onLoadContinuation;
    var _pixels = [];

    function _onMazeLoad(allPixels) {
        var i;
        // Only need Red values to know if the pixel is black or white
        for (i = 0; i < allPixels.length; i = i + 1) {
            if ((i % 4) === 0) {
                _pixels.push(allPixels[i]);
            }
        }
        _onLoadContinuation();
    }

    function _isWall(x, y) {
        if ((x < 0) || (x >= _width) || (y < 0) || (y >= _height)) {
            return null;
        } else {
            return (_pixels[y * _width + x] === 0);
        }
    }

    //  Return null means at maze boundary
    //  Returns a list of each direction where there is a space
    function _getSurroundingSpaces(x, y) {
        var spaces = [];
        if (x <= 0 || x >= _width || y <= 0 || y >= _height) {
            return null;
        }
        if (!_isWall(x, y + 1)) { spaces.push('s'); }
        if (!_isWall(x - 1, y)) { spaces.push('w'); }
        if (!_isWall(x + 1, y)) { spaces.push('e'); }
        if (!_isWall(x, y - 1)) { spaces.push('n'); }
        return spaces;
    }

    return {

        load: function(fn, width, height, callBack) {
            _width = width;
            _height = height;
            _onLoadContinuation = callBack;
            PNG.decode(fn, _onMazeLoad);
        },

        isWall: _isWall,

        getSurroundingSpaces: _getSurroundingSpaces,

    };
})();
