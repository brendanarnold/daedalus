// Singleton pattern from http://www.youtube.com/watch?feature=player_detailpage&v=hQVTIJBZook#t=1701
module.exports = (function() {

    var _ = require('underscore');
    var _guidingDirns = [];
    var _heading;

    function _getOppDirn(dirn) {
        var oppDirn;
        if (dirn === 'n') { oppDirn = 's'; }
        else if (dirn === 's') { oppDirn = 'n'; }
        else if (dirn === 'e') { oppDirn = 'w'; }
        else if (dirn === 'w') { oppDirn = 'e'; }
        else { return null; }
        return oppDirn;
    }

    return {

        name: 'ICARUS',

        pos: [0, 0], // x, y

        updatePos: function(maze) {
            if (_guidingDirns.length > 0) {
                _heading = _guidingDirns.shift();
                // Move based on heading
                if (_heading === 'n') {
                    this.pos[1] = this.pos[1] - 1;
                } else if (_heading === 'w') {
                    this.pos[0] = this.pos[0] - 1;
                } else if (_heading === 'e') {
                    this.pos[0] = this.pos[0] + 1;
                } else if (_heading === 's') {
                    this.pos[1] = this.pos[1] + 1;
                }
            }
        },

        // These are to be updated by tracking my movements by mobile
        // phone
        updateGuidingDirns : function(dirn) {
            _guidingDirns.push(dirn);
        },

        // Useful for debugging
        getGuidingDirns : function() {
            return _guidingDirns;
        },

    }

})();


