// Singleton pattern from http://www.youtube.com/watch?feature=player_detailpage&v=hQVTIJBZook#t=1701
module.exports = (function() {

    var _ = require('underscore');
    var _heading = 's';

    function _getOppDirn(dirn) {
        var oppDirn;
        if (dirn === 'n') { oppDirn = 's'; }
        else if (dirn === 's') { oppDirn = 'n'; }
        else if (dirn === 'e') { oppDirn = 'w'; }
        else if (dirn === 'w') { oppDirn = 'e'; }
        else { return null; }
        return oppDirn;
    }

    return {

        name: 'MINOTAUR',

        pos: [0, 0], // x, y

        updatePos: function(maze) {
            // Minotaur moves along a path, if he gets the option he
            // takes a random direction, but not backwards
            var spaces = maze.getSurroundingSpaces(this.pos[0], this.pos[1]),
                 oppDirn;

            if (spaces === null) {
                // Minataur escaped!
                return (null);
            }
            var numSpaces = spaces.length;
            if (numSpaces === 1) {
                // Situation: Dead end, turn around
                _heading = spaces[0];
            } else {
                // Situation: passage, continue onwards, around corners
                // etc.
                // One heading will be back the way the minotaur came - choose one of the
                // other headings
                oppDirn = _getOppDirn(_heading);
                _heading = _.sample(_.without(spaces, oppDirn));
            }
            // Move based on heading
            if (_heading === 'n') {
                this.pos[1] = this.pos[1] - 1;
            } else if (_heading === 'w') {
                this.pos[0] = this.pos[0] - 1;
            } else if (_heading === 'e') {
                this.pos[0] = this.pos[0] + 1;
            } else if (_heading === 's') {
                this.pos[1] = this.pos[1] + 1;
            }
        }

    }

})();
