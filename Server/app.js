var app = require("http").createServer(processHttp);
var io = require("socket.io").listen(app);
var querystring = require("querystring");
var minotaur = require("./minotaur.js");
var daedalus = require("./daedalus.js");
var icarus = require("./icarus.js");
var sun = require("./sun.js");
var maze = require("./maze.js");
var fs = require("fs");
var path = require("path");


var THE_FLOW_OF_TIME = 2000;

// var MAZE_FN = __dirname + '/maze_debug.png';
// var MAZE_WIDTH = 55;
// var MAZE_HEIGHT = 47;
// var DAEDALUS_START = [100, 100];
// var MINOTAUR_START = [1, 1];

var MAZE_FN = __dirname + "/maze.png";
var MAZE_WIDTH = 4001;
var MAZE_HEIGHT = 4001;

var DAEDALUS_START = [1057, 827];
var ICARUS_START = [1057, 827];
var MINOTAUR_START = [1933, 2971];
// Sun position relative to crete as a whole between 0 and 1,
// -1 is off map, pixels figured out client-side
var SUN_START = [-1.0, 0.5];

// The names of the actors used in the data transfer
var DAEDALUS_NAME = "daedalus";
var ICARUS_NAME = "icarus";
var MINOTAUR_NAME = "minotaur"; // Not yet used
var SUN_NAME = "sun";

var TABLET_OF_RECORDS = __dirname + "/record.json";

// Names for the query parameters used in http POST request
var GUIDANCE_DIRN_POST_KEY = "direction";
var GUIDANCE_SECRET_POST_KEY = "secret";
var ACTOR_NAME_POST_KEY = "actor";

// A secret parameter to make sure spoofing does not happen
var SECRET = "Booyah";
var PORT = process.env.PORT || 8000;
// Limit the numbers of bytes in the post
var MAX_INCOMING_GUIDANCE_BYTES = 1000;

// Create the servers and activate
app.listen(PORT, null, null, function() {
  console.log("Maze loaded");
});
maze.load(MAZE_FN, MAZE_WIDTH, MAZE_HEIGHT, onMazeLoad);

// HTTP POST SERVER - Used to collect directions from people

function processHttp(req, res) {
  var dat = "";

  if (req.method !== "POST") {
    // Some browsers e.g. Chrome, automatically follow up with a GET request
    res.end("DAEDALUS");
  }
  // Read in data
  req.on("data", function(chunk) {
    dat += chunk;
    if (dat.length > MAX_INCOMING_GUIDANCE_BYTES) {
      // Hopefully prevent overflow attacks ...
      return;
    }
  });
  req.on("end", function() {
    var q, guidance;
    try {
      q = querystring.parse(dat);
    } catch (e) {
      return;
    }
    // Some basic data integrety tests
    if (
      typeof q[GUIDANCE_DIRN_POST_KEY] === "undefined" ||
      typeof q[ACTOR_NAME_POST_KEY] === "undefined" ||
      typeof q[GUIDANCE_SECRET_POST_KEY] === "undefined" ||
      q[GUIDANCE_SECRET_POST_KEY] !== SECRET
    ) {
      return;
    }
    // Send the guidance on to the actor
    guidance = q[GUIDANCE_DIRN_POST_KEY];
    actor = q[ACTOR_NAME_POST_KEY];
    if (
      guidance === "n" ||
      guidance === "s" ||
      guidance === "e" ||
      guidance === "w"
    ) {
      if (actor === DAEDALUS_NAME) {
        daedalus.updateGuidingDirns(guidance);
        console.log(daedalus.getGuidingDirns());
      } else if (actor === ICARUS_NAME) {
        icarus.updateGuidingDirns(guidance);
        console.log(icarus.getGuidingDirns());
      }
      res.writeHead(200);
      res.end();
    }
  });
}

// WEBSOCKET SERVER - Used to broadcast movements

function onMazeLoad() {
  
  // Put actors in starting positions
  minotaur.pos = MINOTAUR_START;
  daedalus.pos = DAEDALUS_START;
  icarus.pos = ICARUS_START;
  sun.nrmPos = SUN_START;
  // Get the persisted positions if any
  if (fs.existsSync(TABLET_OF_RECORDS)) { 
    try {
      var record = require(TABLET_OF_RECORDS);
      minotaur.pos = record.minotaur.pos;
      daedalus.pos = record.daedalus.pos;
      icarus.pos = record.icarus.pos;
    } catch (error) {
      console.log("Could not read the tablet of records");
    }
  }

  function persistPositions() {
    fs.writeFile(TABLET_OF_RECORDS, JSON.stringify({
      minotaur: { pos: minotaur.pos },
      daedalus: { pos: daedalus.pos },
      icarus: { pos: icarus.pos }
    }), function (err) {
      console.log("Wrote record to tablet")
    })
  }

  // Set actors in motion and keep record of their movements
  setInterval(function() {
    // Calculate new positions
    minotaur.updatePos(maze);
    daedalus.updatePos(maze);
    icarus.updatePos(maze);
    sun.updateNrmPos();

    daedalus.reactTo(minotaur);

    // Persist the data - not critical, fire and forget
    // Don't need to record sun since its position is deterministic
    persistPositions();
  }, THE_FLOW_OF_TIME);

  // Broadcast movements to watchers
  setInterval(function() {
    io.sockets.emit("posUpdate", {
      daedalusPosX: daedalus.pos[0],
      daedalusPosY: daedalus.pos[1],
      minotaurPosX: minotaur.pos[0],
      minotaurPosY: minotaur.pos[1],
      icarusPosX: icarus.pos[0],
      icarusPosY: icarus.pos[1],
      sunNrmPosX: sun.nrmPos[0],
      sunNrmPosY: sun.nrmPos[1]
    });
    console.log(daedalus.pos);
  }, THE_FLOW_OF_TIME);

  console.log("Updates begun");
}
