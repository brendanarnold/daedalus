
var EARSHOT = 50; // About 600 podes

// Singleton pattern from http://www.youtube.com/watch?feature=player_detailpage&v=hQVTIJBZook#t=1701
module.exports = (function() {

    var _ = require('underscore');
    var _heading = 's';
    var _guidingDirns = ['n', 'e', 's', 'w'];
    var _pos = [0, 0]; // x, y

    function _getOppDirn(dirn) {
        var oppDirn;
        if (dirn === 'n') { oppDirn = 's'; }
        else if (dirn === 's') { oppDirn = 'n'; }
        else if (dirn === 'e') { oppDirn = 'w'; }
        else if (dirn === 'w') { oppDirn = 'e'; }
        else { return null; }
        return oppDirn;
    }

    function _canHear(actor) {
        var dx = this.pos[0] - actor.pos[0];
        var dy = this.pos[1] - actor.pos[1];
        return Math.sqrt(dx * dx + dy * dy) < EARSHOT;
    }


    function _reactToMinotaur(minotaur) {
        if (_canHear.bind(this)(minotaur)) {
            console.info('I can hear the minotaur');
        }
    }

    return {

        name: 'DAEDALUS',

        pos: _pos,

        reactTo(minotaur) {
            _reactToMinotaur.bind(this)(minotaur);
        },


        updatePos: function(maze) {
            // Daedalus moves along a path, if he gets the option he
            // takes a direction from the _guidingDirns stack.
            // He does not go backwards.
            var spaces = maze.getSurroundingSpaces(this.pos[0], this.pos[1]),
                 oppDirn, validDirns;

            if (spaces === null) {
                // Daedalus escaped!
                return (null);
            }
            var numSpaces = spaces.length;
            if (numSpaces === 1) {
                // Situation: Dead end, turn around
                _heading = spaces[0];
            } else {
                // Situation: passage, continue onwards, around corners
                // etc.
                // One heading will be back the way the Daedalus came - choose one of the
                // other headings
                oppDirn = _getOppDirn(_heading);
                validDirns = _.without(spaces, oppDirn);
                //// Returns the first _guidingDirns that is in validDirns
                // _heading = _.find(_guidingDirns, function(d) { return _.contains(validDirns, d); });
                // Returns a random direction
                _heading = _.sample(validDirns);
                // Reduce the influence
                if (validDirns.length > 1 && _guidingDirns.length > 4 
                    && _.contains(validDirns, _guidingDirns[0])) {
                    _guidingDirns.shift();
                }
                
            }
            // Move based on heading
            if (_heading === 'n') {
                this.pos[1] = this.pos[1] - 1;
            } else if (_heading === 'w') {
                this.pos[0] = this.pos[0] - 1;
            } else if (_heading === 'e') {
                this.pos[0] = this.pos[0] + 1;
            } else if (_heading === 's') {
                this.pos[1] = this.pos[1] + 1;
            }
        },

        // These are to be updated by tracking my movements by mobile
        // phone
        updateGuidingDirns : function(dirn) {
            // _guidingDirns = _.without(_guidingDirns, dirn);
            // _guidingDirns.unshift(dirn);
            _guidingDirns.unshift(dirn);
        },

        // Useful for debugging
        getGuidingDirns : function() {
            return _guidingDirns;
        },

    }

})();

